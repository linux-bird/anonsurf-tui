---
# Anonsurf-tui Debian Parrot linux (kali) --->
---

Need [anonsurf](https://gitlab.com/anoopmsivadas/debian-anonsurf) or [kali-anonsurf](https://github.com/Und3rf10w/kali-anonsurf) and [Macchanger](https://pkgs.org/download/macchanger) installed.


Change macchanger 'eth0', with your network interface.  ($ip link)

_as root - when installed as deb -_

`$ nano /usr/bin/anonsurf-macc-tui-0.1-1.sh`

lxterminal is used in the script for anonsurf status ::: Change it to your fav terminal ...

`$ apt install lxterminal`

---

###### Simply run
`$ ./anonsurf-macc-tui-0.1-1.sh`

---





##### Or install .deb file. 

- anonsurf .deb - only anonsurf -->  usr/bin

- anonsurf-macc .deb - anonsurf with macchanger --> usr/bin

`$ sudo dpkg -i *.deb file`

##### Copy *.desktop :: global ::

`$ cp anonsurf-macc-tui.desktop  /usr/share/applications/`



---

