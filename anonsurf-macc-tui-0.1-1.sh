#!/usr/bin/env bash


####################################################
# by oToGamez edit by mus.vogel                    #
# www.pro-toolz.net : Internet Archive             #
####################################################
## ---------------------------------------------- ##
## --- need anonsurf_parrot (kali) installed ---- ##
## ---------------------------------------------- ##
####################################################

## OPTIONAL DEPS Debian ##
# ttf-ancient-fonts-symbola fonts-symbola fonts-noto-color-emoji  neofetch 

job=$!


      E='echo -e';e='echo -en';trap "R;exit" 2
    ESC=$( $e "\e")
   TPUT(){ $e "\e[${1};${2}H";}
  CLEAR(){ $e "\ec";}
  CIVIS(){ $e "\e[?25l";}
   DRAW(){ $e "\e%@\e(0";}
  WRITE(){ $e "\e(B";}
   MARK(){ $e "\e[7m";}
 UNMARK(){ $e "\e[27m";}
      R(){ CLEAR ;stty sane;$e "\ec\e[1;32m\e[J";};
   HEAD(){ DRAW
           for each in $(seq 1 21);do
           $E "      x💠x                                      x💠x"
           done
           WRITE;MARK;TPUT 1 17
           $E $'\e[1;32m'Manage Anonsurf:  ⬇/⬆ Enter $'\e[0m';UNMARK;}
           i=0; CLEAR; CIVIS;NULL=/dev/null
   FOOT(){ MARK;TPUT 22 8
           printf "⚛ ⚛ ⚛ ⚛ ⚛ ⚛ ⚛ ⚛ ⚛ ⚛ ⚛ ⚛ ⚛ ⚛ ⚛ ⚛ ⚛ ⚛ ⚛ ⚛ ⚛ ⚛ ";UNMARK;}
  ARROW(){ read -s -n3 key 2>/dev/null >&2
           if [[ $key = $ESC[A ]];then echo up;fi
           if [[ $key = $ESC[B ]];then echo dn;fi;}
     M0(){  TPUT  2 17; $e "";}
     M1(){  TPUT  3 17; $e "Start";}
     M2(){  TPUT  4 17; $e "Stop";}
     M3(){  TPUT  5 17; $e "Restart";}
     M4(){  TPUT  6 17; $e "Status";}
     M5(){  TPUT  7 17; $e "";}
     M6(){  TPUT  8 17; $e "Enable boot";}
     M7(){  TPUT  9 17; $e "Disable boot";}
     M8(){  TPUT 10 17; $e "Status boot";}
     M9(){  TPUT 11 17; $e "";}
     M10(){ TPUT 12 17; $e "Change ID";}
     M11(){ TPUT 13 17; $e "My IP ";}
     M12(){ TPUT 14 17; $e "";}
     M13(){ TPUT 15 17; $e "Show MAC";}
     M14(){ TPUT 16 17; $e "Random MAC";}
     M15(){ TPUT 17 17; $e "Random Vendor any";}
     M16(){ TPUT 18 17; $e "System info        ";}
     M17(){ TPUT 19 41; $e "EXIT »";}
      LM=17
   MENU(){ for each in $(seq 0 $LM);do M${each};done;}
    POS(){ if [[ $cur == up ]];then ((i--));fi
           if [[ $cur == dn ]];then ((i++));fi
           if [[ $i -lt 0   ]];then i=$LM;fi
           if [[ $i -gt $LM ]];then i=0;fi;}
REFRESH(){ after=$((i+1)); before=$((i-1))
           if [[ $before -lt 0  ]];then before=$LM;fi
           if [[ $after -gt $LM ]];then after=0;fi
           if [[ $j -lt $i      ]];then UNMARK;M$before;else UNMARK;M$after;fi
           if [[ $after -eq 0 ]] || [ $before -eq $LM ];then
           UNMARK; M$before; M$after;fi;j=$i;UNMARK;M$before;M$after;}
   INIT(){ R;HEAD;FOOT;MENU;}
     SC(){ REFRESH;MARK;$S;$b;cur=`ARROW`;}
     ES(){ MARK;$e "                  ✔ ENTER = Menu ";$b;read;INIT;};INIT
  while [[ "$O" != " " ]]; do case $i in
        0) S=M0;SC;if [[ $cur == "" ]];then R;$e "\n$()\n";ES;fi;;
        1) S=M1;SC;if [[ $cur == "" ]];then R;$e "\n$(anonsurf start )\n";ES;fi;;
        2) S=M2;SC;if [[ $cur == "" ]];then R;$e "\n$(anonsurf stop )\n";ES;fi;;
        3) S=M3;SC;if [[ $cur == "" ]];then R;$e "\n$(anonsurf restart)\n";ES;fi;;
        4) S=M4;SC;if [[ $cur == "" ]];then R;$e "\n$(lxterminal -e anonsurf status)\n";ES;fi;;
        5) S=M5;SC;if [[ $cur == "" ]];then R;$e "\n$()\n";ES;fi;;
        6) S=M6;SC;if [[ $cur == "" ]];then R;$e "\n$(anonsurf enable-boot)\n";ES;fi;;
        7) S=M7;SC;if [[ $cur == "" ]];then R;$e "\n$(anonsurf disable-boot)\n";ES;fi;;
        8) S=M8;SC;if [[ $cur == "" ]];then R;$e "\n$(anonsurf status-boot) \n";ES;fi;;
        9) S=M9;SC;if [[ $cur == "" ]];then R;$e "\n$()\n";ES;fi;;
      10) S=M10;SC;if [[ $cur == "" ]];then R;$e "\n$(anonsurf changeid)\n";ES;fi;;
      11) S=M11;SC;if [[ $cur == "" ]];then R;$e "\n$(anonsurf myip)\n";ES;fi;;
	    12) S=M12;SC;if [[ $cur == "" ]];then R;$e "\n$()\n";ES;fi;;
      13) S=M13;SC;if [[ $cur == "" ]];then R;$e "\n$(macchanger -s eth0)\n";ES;fi;;
	    14) S=M14;SC;if [[ $cur == "" ]];then R;$e "\n$(sudo macchanger -r eth0)\n";ES;fi;;
	    15) S=M15;SC;if [[ $cur == "" ]];then R;$e "\n$(sudo macchanger -A eth0)\n";ES;fi;;
	    16) S=M16;SC;if [[ $cur == "" ]];then R;$e "\n$(neofetch)\n";ES;fi;;
	    17) S=M17;SC;if [[ $cur == "" ]];then R;exit 0;fi;;
 esac;POS;done
